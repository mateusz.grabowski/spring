package mat.spring.springmvcrest;


import mat.spring.springmvcrest.web.dto.Repo;
import mat.spring.springmvcrest.web.services.GithubServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SpringMvcRestApplicationTests {

	@Autowired
	GithubServiceImpl github;


	@Test
	public void checkNameOnLocal() {
		Date date = new Date();
		Repo repo = new Repo("fullname","description","cloneurl",10L, date);
		System.out.println("full name = "+repo.getFull_name());
		Assert.assertEquals("fullname", repo.getFull_name());
	}

	@Test
	public void checkNameOnline() {
		System.out.println("full name from response = "+github.findRepoByOwnerAndRepoName("octokit", "octokit.rb").getFull_name());
		Assert.assertEquals("octokit/octokit.rb", github.findRepoByOwnerAndRepoName("octokit", "octokit.rb").getFull_name());
	}

}
