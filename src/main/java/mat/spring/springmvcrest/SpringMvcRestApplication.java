package mat.spring.springmvcrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcRestApplication.class, args);
	}

}
