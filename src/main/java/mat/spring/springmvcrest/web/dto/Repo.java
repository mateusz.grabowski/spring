package mat.spring.springmvcrest.web.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.Date;

public class Repo {


    private String full_name;
    private String description;
    private String clone_url;
    private Long stargazers_count;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private Date created_at;


    public Repo() {
    }

    public Repo(String full_name, String description, String cloneUrl, Long stars, Date created_at) {
        this.full_name = full_name;
        this.description = description;
        this.clone_url = cloneUrl;
        this.stargazers_count = stars;
        this.created_at = created_at;
    }

    @JsonGetter("fullName")
    public String getFull_name() {
        return full_name;
    }

    @JsonSetter("full_name")
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("cloneUrl")
    public String getClone_url() {
        return clone_url;
    }

    @JsonSetter("clone_url")
    public void setClone_url(String clone_url) {
        this.clone_url = clone_url;
    }

    @JsonGetter("stars")
    public Long getStargazers_count() {
        return stargazers_count;
    }

    @JsonSetter("stargazers_count")
    public void setStargazers_count(Long stargazers_count) {
        this.stargazers_count = stargazers_count;
    }


    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

//        public Date getCreatedAt() {
//        return created_at;
//    }
//
//    public void setCreatedAt(Date createdAt) {
//        this.created_at = createdAt;
//    }
}
