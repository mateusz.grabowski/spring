package mat.spring.springmvcrest.web.dto;

public class Person {
    private Integer id;
    private String userName;
    private String name;

    public Person(){

    }

    public Person(Integer id, String userName){
        this.id = id;
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
