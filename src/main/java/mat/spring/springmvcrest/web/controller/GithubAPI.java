package mat.spring.springmvcrest.web.controller;

import mat.spring.springmvcrest.web.dto.Repo;
import mat.spring.springmvcrest.web.services.GithubService;
import org.springframework.web.bind.annotation.*;

@RestController
public class GithubAPI {

    private GithubService githubService;

    public GithubAPI(GithubService githubService) {
        this.githubService = githubService;
    }

    @RequestMapping(value = "/repositories/{owner}/{repositoryName}", method = RequestMethod.GET,headers = "Accept=application/vnd.github.v3+json")
    public @ResponseBody Repo getById(
            @PathVariable String owner,@PathVariable String repositoryName) {
        return githubService.findRepoByOwnerAndRepoName(owner, repositoryName);
    }



}
