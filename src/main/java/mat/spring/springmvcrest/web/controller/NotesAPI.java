package mat.spring.springmvcrest.web.controller;

import mat.spring.springmvcrest.web.dto.Notes;
import mat.spring.springmvcrest.web.repositories.NotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class NotesAPI {

    @Autowired
    private NotesRepository notesRepository;


    @GetMapping("/notes/{id}")
    public @ResponseBody
    Optional<Notes>
    getById(@PathVariable Long id) {
        return notesRepository.findById(id);
    }

    @GetMapping("/notes/add")
    public @ResponseBody String addNotes (@RequestParam String title,
                                          @RequestParam String description){
        Notes n = new Notes();
        n.setTitle(title);
        n.setDescription(description);
        notesRepository.save(n);
        return "Saved";
    }

    @GetMapping("notes/remove/{id}")
    public void removeNote(@PathVariable Long id){
        notesRepository.deleteById(id);
    }




    @GetMapping("/notes/all")
    public @ResponseBody Iterable<Notes> getAllNotes(){
        return notesRepository.findAll();
    }



}
