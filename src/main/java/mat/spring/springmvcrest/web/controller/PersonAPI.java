package mat.spring.springmvcrest.web.controller;

import mat.spring.springmvcrest.web.dto.Person;
import mat.spring.springmvcrest.web.services.PersonService;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonAPI {


    private PersonService personService;

    public PersonAPI(PersonService personService) {
        this.personService = personService;
    }


    @GetMapping("/users/{id}")
    public @ResponseBody Person
    getById(@PathVariable Integer id) {
        return personService.findPersonById(id);
    }


    @PostMapping(value = "/createPerson", consumes = "application/json", produces = "application/json")
    public Person createPerson(@RequestBody Person person){
        return personService.saveUpdatedPerson(person);
    }


}
