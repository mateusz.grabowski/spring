package mat.spring.springmvcrest.web.services;

import mat.spring.springmvcrest.web.dto.Notes;
import mat.spring.springmvcrest.web.repositories.NotesRepository;

import java.util.List;

public class NotesServiceImpl implements NotesService{

    //private final RestTemplate restTemplate;
    private final NotesRepository notesRepository;

    public NotesServiceImpl(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    @Override
    public Notes findNotesById(Long id) {
        return notesRepository.getOne(id);
    }

    @Override
    public List<Notes> findAllNotes() {
        return notesRepository.findAll();
    }

    @Override
    public void removeNote(Long id) {
        notesRepository.deleteById(id);
    }



}
