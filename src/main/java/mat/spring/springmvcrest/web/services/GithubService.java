package mat.spring.springmvcrest.web.services;

import mat.spring.springmvcrest.web.dto.Repo;

public interface GithubService {


    Repo findRepoByOwnerAndRepoName(String Owner, String RepoName);


}
