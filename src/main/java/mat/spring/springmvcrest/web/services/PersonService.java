package mat.spring.springmvcrest.web.services;

import mat.spring.springmvcrest.web.dto.Person;

public interface PersonService {

    public Person saveUpdatedPerson(Person person);

    public Person findPersonById(Integer id);

}
