package mat.spring.springmvcrest.web.services;

import mat.spring.springmvcrest.web.dto.Repo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GithubServiceImpl implements GithubService {
    private static final String URL = "https://api.github.com/repos";
    private static final String URL_SEP = "/";
    private final RestTemplate restTemplate;

    public GithubServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }



    @Override
    public Repo findRepoByOwnerAndRepoName(String Owner, String RepoName) {
        ResponseEntity<Repo> resp = restTemplate.getForEntity(URL+URL_SEP+Owner+URL_SEP+RepoName,Repo.class);
        return resp.getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
    }
}
