package mat.spring.springmvcrest.web.services;


import mat.spring.springmvcrest.web.dto.Notes;

import java.util.List;

public interface NotesService {

    Notes findNotesById(Long id);

    List<Notes> findAllNotes();

    void removeNote(Long id);
}
