package mat.spring.springmvcrest.web.services;

import mat.spring.springmvcrest.web.dto.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PersonServiceImpl implements PersonService {
    private static final String URL = "https://jsonplaceholder.typicode.com/users";
    private static final String URL_SEP = "/";

    private final RestTemplate restTemplate;

    public PersonServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Person saveUpdatedPerson(Person person) {
        return person;
    }


    @Override
    public Person findPersonById(Integer id) {
            ResponseEntity<Person> resp = restTemplate.getForEntity(URL+URL_SEP+id,Person.class);
            return resp.getStatusCode() == HttpStatus.OK ? resp.getBody() : null;
    }


}
