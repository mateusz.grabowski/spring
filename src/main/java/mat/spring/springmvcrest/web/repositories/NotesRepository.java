package mat.spring.springmvcrest.web.repositories;

import mat.spring.springmvcrest.web.dto.Notes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface NotesRepository extends JpaRepository<Notes, Long > {
}
