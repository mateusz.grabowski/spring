//package mat.spring.springmvcrest.controllers;
//
//
//import mat.spring.springmvcrest.domain.User;
//import mat.spring.springmvcrest.services.UserService;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping(UserController.BASE_URL)
//public class UserController {
//
//    static final String BASE_URL = "/api/v1/users";
//
//    private final UserService userService;
//
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }
//
//    @GetMapping
//    List<User> getAllUsers(){
//        return userService.findAllUsers();
//    }
//
//    @GetMapping("/{id}")
//    public User getUserById(@PathVariable Long id){
//        return userService.findUserById(id);
//    }
//
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public User saveUser(@RequestBody User user){
//        return userService.saveUser(user);
//    }
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.ACCEPTED)
//    public void deleteUser(@PathVariable Long id){
//        userService.deleteUser(id);
//    }
//
//    @PutMapping(path="/update")
//    public @ResponseBody String updateUser(@RequestBody User user) {
//        userService.saveUser(user);
//        return "Updated"; }
//
//
//}
